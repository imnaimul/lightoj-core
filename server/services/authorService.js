import {transaction} from 'objection'

import Problem from '../models/problem'
import ProblemPermission from '../models/problemPermission'
import ProblemDescriptionHistory from '../models/problemDescriptionHistory'

import * as problemService from './problemService'

const PROBLEM_DESCRIPTION_SECTIONS = ['main', 'input', 'output', 'note', 'title']

export async function createProblem (problemTitle, problemHandle, userId) {
  const knex = Problem.knex()
  let trx
  try {
    trx = await transaction.start(knex)

    const problemData = {
      'problemTitleStr': problemTitle,
      'problemHandleStr': problemHandle,
      'userId': userId
    }

    const problemTrx = await Problem
      .query(trx)
      .insert(problemData)

    const problemDescriptionBaseData = {
      'descTypeStr': 'main',
      'descMarkdownStr': '',
      'descStateStr': 'approved',
      'problemId': problemTrx.problemId,
      'userId': userId,
      'approverId': userId,
      'isActiveBool': true
    }

    for (let i = 0; i < PROBLEM_DESCRIPTION_SECTIONS.length; i++) {
      let section = PROBLEM_DESCRIPTION_SECTIONS[i]
      problemDescriptionBaseData.descTypeStr = section
      if (section === 'title') {
        problemDescriptionBaseData.descMarkdownStr = problemTitle
      } else {
        problemDescriptionBaseData.descMarkdownStr = ''
      }
      await ProblemDescriptionHistory.query(trx).insert(problemDescriptionBaseData)
    }

    const problemPermissionData = {
      'userId': userId,
      'problemId': problemTrx.problemId,
      'roleTypeStr': 'owner'
    }

    await ProblemPermission // eslint-disable-line
      .query(trx)
      .insert(problemPermissionData)

    await trx.commit()

    return problemTrx
  } catch (err) {
    await trx.rollback()
    console.log(err)
    return null
  }
}

export async function getProblems (userId, page, limit) {
  const problems = await Problem
    .query()
    .whereIn('problemId', ProblemPermission.query().select('problemId').where('userId', userId))
    .orderBy('problemId')
  return problems
}

export async function getUserProblemRole (userId, problemHandle) {
  const permission = await ProblemPermission.query().select('roleTypeStr').where('userId', userId)
    .whereIn('problemId', Problem.query().select('problemId').where('problemHandleStr', problemHandle))
  if (permission.length > 0) {
    return permission[0].roleTypeStr
  } else {
    return null
  }
}

export async function getProblemData (problemHandle) {
  const problem = await problemService.getProblem(problemHandle)
  if (!problem) return null

  const latestProblemDescription = await problem.$relatedQuery('problemDescriptionHistory').where('isActiveBool', true)

  let problemDescriptionData = {}
  latestProblemDescription.forEach((desc) => {
    problemDescriptionData[desc.descTypeStr] = desc
  })
  problem.description = problemDescriptionData

  return problem
}

export async function updateProblemDescription (problemHandle, newProblemData, userId) {
  const currentProblemData = await getProblemData(problemHandle)
  const problemId = currentProblemData.problemId

  if (!currentProblemData) return null

  const knex = ProblemDescriptionHistory.knex()
  let trx
  try {
    trx = await transaction.start(knex)

    for (let i = 0; i < PROBLEM_DESCRIPTION_SECTIONS.length; i++) {
      let section = PROBLEM_DESCRIPTION_SECTIONS[i]
      let newData = newProblemData.description[section]
      let oldData = currentProblemData.description[section]

      if (oldData.descMarkdownStr !== newData.descMarkdownStr) {
        const problemDescriptionData = {
          'descTypeStr': section,
          'descMarkdownStr': newData.descMarkdownStr,
          'descStateStr': 'approved',
          'problemId': problemId,
          'userId': userId,
          'approverId': userId,
          'isActiveBool': true
        }
        await ProblemDescriptionHistory.query(trx).insert(problemDescriptionData).debug()
        await ProblemDescriptionHistory.query(trx).patch({'isActiveBool': false})
          .where('descHistoryId', oldData.descHistoryId).debug()
        if (section === 'title') {
          await Problem.query(trx).patch({'problemTitleStr': problemDescriptionData.descMarkdownStr})
            .where('problemId', problemId)
        }
      }
    }
    await trx.commit()
  } catch (err) {
    await trx.rollback()
    console.log('Something went wrong ', err)
    return null
  }
  return getProblemData(problemHandle)
}
