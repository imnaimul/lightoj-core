import SocialAuth from '../models/socialAuth'
import * as userService from './userService'

export async function create (data) {
  let socialAuth = await SocialAuth.query().insert(data)
  return socialAuth
}

export async function get (accountId, provider) {
  let socialAuth = await SocialAuth.query().where('accountId', accountId)
    .andWhere('provider', provider).first()
  return socialAuth
}

export async function getOrCreate (data) {
  let socialAuth = await get(data.accountId, data.provider)
  if (!socialAuth) {
    socialAuth = await create(data)
  }
  return socialAuth
}

export async function getUser (accessToken, refreshToken, profile) {
  console.log(profile)
  let user = null
  let socialAuth = {}
  try {
    socialAuth = await get(profile.id, profile.provider)
    if (!socialAuth) {
      user = await userService.getOrCreate({
        fullname: profile.displayName,
        email: profile.emails[0].value,
        handle: profile.emails[0].value,
        password: accessToken
      })
      let userId = user.userId
      await getOrCreate({
        accessToken: accessToken,
        userId: userId,
        refreshToken: refreshToken,
        provider: profile.provider,
        accountId: profile.id,
        profileLink: profile._json.url || profile.profileUrl
      })
    } else {
      user = await userService.get(socialAuth.userId)
    }
  } catch (error) {
    console.log(error)
    throw new Error(error)
  }

  return user
}
