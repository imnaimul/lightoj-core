import Problem from '../models/problem'

export async function checkHandleExist (problemHandle) {
  const res = await Problem.query().where('problemHandleStr', problemHandle)
  return (res.length > 0)
}

export async function getProblem (problemHandle) {
  const res = await Problem.query().where('problemHandleStr', problemHandle)
  console.log(res)
  if (res.length > 0) {
    return res[0]
  }
  return null
}
