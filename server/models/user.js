import BaseModel from './base'

const TABLE_NAME = 'users'

/**
 * User model.
 */
class User extends BaseModel {
  static get tableName () {
    return TABLE_NAME
  }
  static get idColumn () {
    return 'userId'
  }
}

export default User
