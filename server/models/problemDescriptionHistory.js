import BaseModel from './base'

const TABLE_NAME = 'problemDescriptionHistory'

/**
 * User model.
 */
class ProblemDescriptionHistory extends BaseModel {
  static get tableName () {
    return TABLE_NAME
  }
  static get idColumn () {
    return 'descHistoryId'
  }
}

export default ProblemDescriptionHistory
