import {Router} from 'express'
import {check, validationResult} from 'express-validator/check'

import * as authorService from '../services/authorService'
import * as problemService from '../services/problemService'

import {successResponse, errorResponse} from '../utils/response'

const router = Router()

router.get('/problem/:handle', [],
  async (req, res, next) => {
    const role = await authorService.getUserProblemRole(req.user.id, req.params.handle)

    if (!role) {
      return errorResponse(res, 'You are forbidden', 403)
    }

    const problem = await authorService.getProblemData(req.params.handle)

    if (!problem) {
      return errorResponse(res, 'Problem not found', 404)
    }

    const data = {
      role,
      problem
    }
    return successResponse(res, 'OK', data)
  }
)

router.post('/problem/:handle', [
  check('problemData').exists().withMessage('Problem description data is required')
],
async (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return errorResponse(res, errors.array().map(error => error.msg))
  }

  const role = await authorService.getUserProblemRole(req.user.id, req.params.handle)

  if (!role || (role !== 'owner' && role !== 'editor')) {
    return errorResponse(res, 'You are forbidden', 403)
  }

  const problem = await authorService.updateProblemDescription(req.params.handle, req.body.problemData, req.user.id)

  if (!problem) {
    return errorResponse(res, 'Problem not found', 404)
  }

  const data = {
    role,
    problem
  }
  return successResponse(res, [], data)
}
)

router.post(
  '/problem',
  [
    check('problemTitle')
      .exists().withMessage('Title field is required')
      .isLength({min: 1}).withMessage('Minimum 1 character is required for the title field'),
    check('problemHandle')
      .exists().withMessage('Handle field is required')
      .isLength({min: 3}).withMessage('Minimum 3 characters are required for the handle field')
      .custom(async (value, {req, loc, path}) => {
        const exist = await problemService.checkHandleExist(value)
        return (exist === true) ? false : value
      }).withMessage('This problem handle already exists')
  ],
  async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array().map(error => error.msg))
    }
    const problem = await authorService.createProblem(req.body.problemTitle, req.body.problemHandle, req.user.id)
    return successResponse(res, [], problem)
  })

router.get(
  '/problems',
  [],
  async (req, res, next) => {
    const problems = await authorService.getProblems(req.user.id, req.body.page, req.body.limit)
    return successResponse(res, [], problems)
  }
)

export default router
