
exports.up = async function (knex, Promise) {
  await knex.schema.createTable('problemDescriptionHistory', function (t) {
    t.bigIncrements('descHistoryId').unsigned().primary()
    t.bigInteger('problemId').unsigned().notNullable().references('problems.problemId')
    t.string('localIdStr', 5).notNullable().defaultTo('en')
    t.enu('descTypeStr', ['main', 'input', 'output', 'note', 'title'])
    t.enu('descStateStr', ['suggest', 'approved', 'rejected']).defaultTo('suggest')
    t.text('descMarkdownStr').nullable().defaultTo('')
    t.boolean('isActiveBool').notNullable().defaultTo(false)
    t.boolean('isPublishedBool').notNullable().defaultTo(false)

    t.bigInteger('userId').unsigned().notNullable().references('users.userId')
    t.bigInteger('approverId').unsigned().notNullable().references('users.userId')
    t.timestamp('deleted_at').nullable()
    t.timestamps()
  })
}

exports.down = async function (knex) {
  await knex.schema.dropTableIfExists('problemDescriptionHistory')
}
