export const state = () => ({
  locales: ['en', 'bn'],
  locale: 'en'
})

export const mutations = {
  SET_LANG (state, locale) {
    if (state.locales.indexOf(locale) !== -1) {
      state.locale = locale
    }
  }
}

export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    if (req.user) {
      console.log('I am second')
      commit('auth/INIT_LOGIN', req.user)
    }
  }
}
